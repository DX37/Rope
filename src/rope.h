#ifndef ROPE_H
#define ROPE_H

struct rope
{
    int length;
    char *str;
    struct rope *left;
    struct rope *right;
};

typedef struct {
    struct rope *first;
    struct rope *second;
} pair;

pair *pair_init();
struct rope *rope_init();
struct rope *rope_create(char*);
struct rope *rope_merge(struct rope*, struct rope*);
char rope_get_char(struct rope*, int);
pair *rope_split(struct rope*, int);
struct rope *rope_insert(struct rope*, int, char*);
struct rope *rope_delete(struct rope*, int, int);
void rope_print(struct rope*);
void rope_print_levels(struct rope*, int);

#endif // ROPE_H
