#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rope.h"

pair *pair_init()
{
    pair *p;

    if ((p = malloc(sizeof(*p))) == NULL)
        return NULL;

    p->first = NULL;
    p->second = NULL;

    return p;
}

struct rope *rope_init()
{
    struct rope *node;

    if ((node = malloc(sizeof(*node))) == NULL)
        return NULL;

    node->length = 0;
    node->str = NULL;
    node->left = NULL;
    node->right = NULL;

    return node;
}

struct rope *rope_create(char *string)
{
    struct rope *new_rope;
    new_rope = rope_init();

    new_rope->length = strlen(string);
    new_rope->str = (char*)calloc(new_rope->length, sizeof(char));
    new_rope->str = string;

    return new_rope;
}

struct rope *rope_merge(struct rope *node1, struct rope *node2)
{
    struct rope *res = rope_init();

    res->left = node1;
    res->right = node2;
    res->length = node1->length + node2->length;

    return res;
}

char rope_get_char(struct rope *node, int i)
{
    if (node->left != NULL)
        if (node->left->length >= i)
            return rope_get_char(node->left, i);
        else
            return rope_get_char(node->right, i - node->left->length);
    else
        return node->str[i];
}

pair *rope_split(struct rope *node, int i)
{
    struct rope *tree1, *tree2;
    pair *res = pair_init();

    tree1 = rope_init();
    tree2 = rope_init();

    if (node->left != NULL)
        if (node->left->length >= i)
        {
            res = rope_split(node->left, i);
            tree1 = res->first;
            tree2->left = res->second;
            tree2->right = node->right;
            tree2->length = tree2->left->length + tree2->right->length;
        }
        else
        {
            res = rope_split(node->right, i - node->left->length);
            tree1->left = node->left;
            tree1->right = res->first;
            tree1->length = tree1->left->length + tree1->right->length;
            tree2 = res->second;
        }
    else
    {
        tree1->str = (char*)calloc(i, sizeof(char));
        tree2->str = (char*)calloc(strlen(node->str), sizeof(char));
        strncpy(tree1->str, node->str, i);
        strncpy(tree2->str, node->str+i, strlen(node->str));
        tree1->length = i;
        tree2->length = strlen(node->str) - i;
    }

    res->first = tree1;
    res->second = tree2;

    return res;
};

struct rope *rope_insert(struct rope *node, int insert_i, char *string)
{
    struct rope *tree1, *tree2, *tree3;
    pair *res = pair_init();

    tree1 = rope_init();
    tree2 = rope_init();
    tree3 = rope_init();

    res = rope_split(node, insert_i);
    tree1 = res->first;
    tree3 = res->second;

    tree2 = rope_create(string);

    return rope_merge(rope_merge(tree1, tree2), tree3);
}

struct rope *rope_delete(struct rope *node, int begin_i, int end_i)
{
    struct rope *tree1, *tree2, *tree3;
    pair *res = pair_init();

    tree1 = rope_init();
    tree2 = rope_init();
    tree3 = rope_init();

    res = rope_split(node, begin_i);
    tree1 = res->first;
    tree2 = res->second;

    tree3 = rope_split(tree2, end_i - begin_i)->second;

    return rope_merge(tree1, tree3);
}

void rope_print(struct rope *node)
{
    if (!node)
        return;

    if (node->str != NULL)
        printf("%s", node->str);

    rope_print(node->left);
    rope_print(node->right);
}

void rope_print_levels(struct rope *node, int i)
{
    if (!node)
        return;

    for (int x = 0; x < i; ++x)
        printf("  ");
    if (node->str == NULL)
        printf("%d: NODE - %d\n", i, node->length);
    else
        printf("%d: %s - %d\n", i, node->str, node->length);
    ++i;

    rope_print_levels(node->left, i);
    rope_print_levels(node->right, i);
}
