#include <stdio.h>
#include "rope.h"

int main()
{
    struct rope *root, *r1, *r2;
    root = rope_init();
    r1 = rope_init();
    r2 = rope_init();

    r1 = rope_create("Let's test th");
    r2 = rope_create("is test!");
    root = rope_merge(r1, r2);

    printf("Original string is: ");
    rope_print(root);
    printf("\n\n");
    rope_print_levels(root, 0);
    printf("\n");

    root = rope_insert(root, 16, "goddamn ");
    rope_print_levels(root, 0);
    printf("\n");
    printf("Resulting string is: ");
    rope_print(root);
    printf("\n");

    root = rope_delete(root, 15, 28);
    rope_print_levels(root, 0);
    printf("\n");
    printf("Resulting string is: ");
    rope_print(root);
    printf("\n");

    return 0;
}
